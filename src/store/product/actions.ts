import { api } from 'src/boot/axios';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { SET_PRODUCT } from './mutations';
import { StateProduct } from './state';

const actions: ActionTree<StateProduct, StateInterface> = {
  async getProducts({ commit }) {
    const result = await api.get('products');

    if (result.status != 200 || result.data == null) {
      alert('something went wrong');
      return;
    }

    commit(SET_PRODUCT, result.data?.products);
  },
};

export default actions;
