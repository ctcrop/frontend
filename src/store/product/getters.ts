import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { StateProduct } from './state';

const getters: GetterTree<StateProduct, StateInterface> = {
  getProducts(state: StateProduct) {
    return state.products;
  },
};

export default getters;
