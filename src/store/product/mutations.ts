import { MutationTree } from 'vuex';
import { Product, StateProduct } from './state';

export const SET_PRODUCT = 'SET_PRODUCT';

const mutation: MutationTree<StateProduct> = {
  [SET_PRODUCT](state: StateProduct, products: Product[]) {
    state.products = products;
  },
};

export default mutation;
