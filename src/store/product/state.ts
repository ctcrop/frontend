export interface Product {
  id: number;
  title: string;
  description: string;
  price: number;
  discountPercentage: number;
  rating: number;
  stock: number;
  brand: string;
  category: string;
  thumbnail: string;
  images: string[];
}

export interface StateProduct {
  products: Product[];
}

function state(): StateProduct {
  return {
    products: [],
  };
}

export default state;
