import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { StateProduct } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const productModule: Module<StateProduct, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default productModule;
